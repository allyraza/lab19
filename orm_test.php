<?php

// load the orm
require __DIR__.'/orm.php';

// load the user model
require __DIR__.'/user.php';

class OrmTest extends PHPUnit_Framework_TestCase {

	/**
     * @expectedException InvalidEmailException
     */
	public function test_invalid_email()
	{
		$user = UserModel::create(array(
			'name'		=>'Ally',
			'occupation'=>'Programmer',
			'email'		=>'invalidEmail'
		));
		$user->save();
	}
}