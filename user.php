<?php

// Define a user model and
// extend it from the base model/orm
class UserModel extends Orm {
    
    public $table = 'users';

    public function rules()
    {
      return array(
        'email'=>'email',
      );
    }

}