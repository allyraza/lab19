<?php
/*
  PHP interview task for Lab19.

  Requires PHP 5.4

  ORM/UserModel class
  - Create an ORM class demonstrating CRUD.
  - The class should store data in sqlite/mysql database.
  - Proper security of the database should be demonstrated.
  - The UserModel class should extend a base ORM class.
  - The UserModel class should specify a validator for the
    email attribute, to make sure the email is valid, otherwise
    it should throw a catchable error.

  Unit test
  - Get PHPUnit via composer.
  - Create just one unit test that tests the email validation script.

  When you're done:
  - dump the sql file (if you're using mysql), and place in the root.
  - git push the codebase to a new public repo on Bitbucket, and 
    share the link with us.

*/

// load the config, and the orm
$config = require __DIR__.'/config.php';
require __DIR__.'/orm.php';
Orm::connect($config);

// load the user model class
require __DIR__.'/user.php';


// The following should be available:
// Ability to create a new user model.
$user = UserModel::create(array(
  'name' => 'Luke',
  'occupation' => 'Programmer',
  'email' => 'luke@gmail.com'
));

// Saves to database
$user->_save();

/*
  Note that methods for every attribute of the given model should be generated 
  DYNAMICALLY/ON THE FLY based on schema, using PHP closures or other methods.
*/
echo $user->name(); // outputs "Luke"
echo "<br>";
echo $user->occupation(); // outputs "Programmer"

// Sets the name, occupations, updates
$user->name('Paul');
$user->occupation('Designer');
$user->_save();

echo "<br>";
echo $user->name(); // outputs "Pual"

// Finds the first existing user from the database, updates attributes, saves changes
$currentUser = UserModel::first();
$currentUser->name('Bob');
$currentUser->occupation('Project Manager');

// Saves to database
$currentUser->_save();

// Fails to save new email address
try {
  $currentUser->email('bademail');
  $currentUser->save();
} catch( Exception $e ){
  // Shows email validation error message
  printf("<h3>%s</h3>",  $e->getMessage());
}

// Deletes a user
$currentUser->_delete();
