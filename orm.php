<?php 

class InvalidEmailException extends Exception {};

class Orm {

  /*
   * Register events to the model
   * @var array
   */
  public $filters;

  /*
   * The databsae connection use for quries
   * @var object
   */
  public static $pdo;

  /*
   * Model attributes
   * @var array
   */
  public $attributes;

  /*
   * indicates if the model exists in the database.
   * @var bool
   */
  public $exists;


  public function __construct($attributes = [], $exists = false)
  {
    $this->attributes = [];
    $this->filters = [];
    $this->init();
    $this->exists = $exists;
    $this->fill($attributes);
  }

  /*
  * Populate the model attributes from array
  * @param array $attributes
  * @return void
  */
  public function fill($attributes)
  {
    foreach ($attributes as $key => $value)
    {
      $this->setAttribute($key, $value);
    }
  }

  /*
  * Find the first record
  * @return object
  */
  public static function first()
  {
    $model = new static;
    $result = $model->query("SELECT * FROM {$model->table} LIMIT 1")->fetch(PDO::FETCH_ASSOC);
    $model->exists = true;
    $model->fill($result);
    return $model;
  }

  /*
  * Create a new model and return the object
  * @params array $attributes
  * @return object
  */
  public static function create($attributes)
  {
    $model = new static($attributes);
    return $model;
  }

  /*
  * Run the validations based on the rules specified in the model
  */
  public function validate()
  {
    $rules = $this->rules();
    if ( ! empty($rules) && in_array('email', $rules))
    {
      if ( ! $this->valid() )
      {
        throw new InvalidEmailException("Invalid Email Address.");
      }
    }
  }

  /*
  * Perform a basic email check using PHP's builtin Filters
  * @return bool
  */
  public function valid()
  {
    return filter_var($this->email(), FILTER_VALIDATE_EMAIL) !== false;
  }

  /*
  * Presist the stuff to the database
  * @return bool
  */
  public function save()
  {

    $this->validate();

    if ($this->exists)
    {
      $result = $this->update();
    }
    else
    {
      $this->insert();
      $id = $this->pdo()->lastInsertId();
      $this->setAttribute('id', $id);
      $this->exists = $result = is_numeric($id);
    }
    return $result;
  }

  /*
  * Alias to save see above
  * @return bool
  */
  public function _save()
  {
    return $this->save();
  }

  /*
  * Insert a new record to the database
  * @return bool
  */
  public function insert()
  {
    $columns = implode(', ', array_keys($this->attributes));
    $keys = $this->buildKeys();
    return $this->query("INSERT INTO {$this->table} ({$columns}) VALUES ({$keys})");
  }

  /*
  * Update an existing model
  * @return bool
  */
  private function update()
  {
      $columns = $this->buildKeys(true);
      return $this->query("UPDATE {$this->table} SET {$columns} WHERE id=:id");
  }

  /*
  * Delete current object from database
  * @return bool
  */
  public function delete()
  {
    if ($this->exists)
    {
      $id = $this->getAttribute('id');
      return $this->query("DELETE FROM {$this->table} WHERE id=:id", ['id'=>$id]);
    }
  }
  
  /* 
  * Alias to delete
  * @return bool
  */
  public function _delete()
  {
    $this->delete();
  }

  /*
  * Perform a database query
  * @param string $sql
  * @param array $bindings
  * @return object $pdo
  */
  private function query($sql, $params=[])
  {
    if (empty($params))
    {
      $params = $this->attributes;
    }
    $db = $this->pdo()->prepare($sql);
    $db->execute($params);
    return $db;
  }

  /*
  * Prepare bindings for insert/update query 
  * @param bool $pair
  * @return string
  */
  private function buildKeys($pair=true)
  {
    $keys = [];

    foreach (array_keys($this->attributes) as $key)
    {
      $keys[] = ($pair)?sprintf("%s = :%s", $key, $key):sprintf(":%s", $key);
    }
    return implode(', ', $keys);
  }

  
  /*
  * Get a pdo object
  * @return object $pdo
  */
  private function pdo()
  {
    return static::$pdo;
  }

  /*
  * Connect to the database using pdo
  * params array $config
  * @return object $pdo
  */
  public static function connect($config)
  {
    $pdo = static::$pdo;
    if ( ! $pdo)
    {
      try
      {
        extract($config);
        $pdo = new PDO("mysql:host={$host};dbname={$database}", $username, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        static::$pdo = $pdo;
      }
      catch (Exception $e)
      {
        echo $e->getMessage();
      }
    }
    return $pdo;
  }

  /*
  * Set a model attribute
  * @param string $key
  * @param mix $value
  * @return void
  */
  public function setAttribute($key, $value)
  {
    $this->attributes[$key] = $value;
  }

  /*
  * Get a model attribute
  * @params string $key
  * @return string
  */
  public function getAttribute($key)
  {
    if (array_key_exists($key, $this->attributes))
    {
      return $this->attributes[$key];
    }
  }

  /*
   * using php's __call magic method to handle dynamic method calls
   * @return string $attribute
   */
  public function __call($method, $args)
  {
    if ( ! empty($args))
    {
      $value = is_array($args)?reset($args):$args;
      $this->setAttribute($method, $value);
    }
    else 
    {
      return $this->getAttribute($method);
    }
  }

} // Orm