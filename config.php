<?php
return array(
	// hostname
	'host'=>'localhost',
	// database name
	'database'=>'blog',
	// database username
	'username'=>'root',
	// database password
	'password'=>'',
	// database encoding
	'charset'=>'utf8'
);